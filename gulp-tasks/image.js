
module.exports = function (args) {
	let gulp = args.gulp, $ = args.$, PATH = args.PATH, argv = args.argv;
	return argv.imagemin ? ()=> gulp.src([`${PATH.IMG.SOURCE}**/*`, `!${PATH.IMG.SOURCE}sprites`, `!${PATH.IMG.SOURCE}sprites/**/*`])
		.pipe($.imagemin([
			$.imagemin.gifsicle({interlaced: true}),
			$.imageminJpegRecompress({
				progressive: true,
				max: 80,
				min: 70
			}),
			$.imageminPngquant({quality: '75-85'}),
			$.imagemin.svgo({plugins: [{removeViewBox: false}]})
		]))
		.pipe(gulp.dest(PATH.IMG.PUBLIC))
		.on('error', function(error){
			$.util.log(error.message);
			this.emit('end');
		}) : ()=> gulp.src([`${PATH.IMG.SOURCE}**/*`, `!${PATH.IMG.SOURCE}sprites`, `!${PATH.IMG.SOURCE}sprites/**/*`])
		.pipe(gulp.dest(PATH.IMG.PUBLIC))
		.on('error', function(error){
			$.util.log(error.message);
			this.emit('end');
		});
};
