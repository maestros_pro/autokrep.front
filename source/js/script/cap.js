import $ from 'jquery'
import Popup from '../modules/module.popup'
import Form from '../modules/module.validate'

window.app = window.app || {};

$(function () {

	window.app.popup = new Popup();
	const form = new Form();

	let $b = $('body');

	$b
		.on('submit', '.form', function (e) {
			var $form = $(this),
				$item = $form.find('input'),
				wrong = false
			;

			$item.each(function () {
				let input = $(this), rule = $(this).attr('data-require');
				$(input)
					.closest('.form__field')
					.removeClass('f-error f-message')
					.find('.form__message')
					.html('')
				;

				if ( rule ){
					form.validate(input[0], rule, err =>{
						if (err.errors.length) {
							wrong = true;
							$(input)
								.closest('.form__field')
								.addClass('f-error f-message')
								.find('.form__message')
								.html(err.errors[0])
							;
						}
					})
				}
			});

			if ( wrong ){
				e.preventDefault();
			}
		})
	;
});


