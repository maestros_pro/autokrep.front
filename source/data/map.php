<?php
header('Access-Control-Allow-Origin: *');

$data = $_POST;


$arr = array(
	array(
		'coordinates' => array(),
		'd' => 4,
		'e' => 5
	)
);

$shortDescription = '
	<div class="object-point">
		<div class="object-point__title">Санкт-Петербург ЛС</div>
		<div class="object-point__address">Латышских Стрелков ул., 31</div>
		<div class="object-point__text"><i class="ico-ring" style="color:red;"></i> Проспект Большевиков</div>
	</div>';

$fullDescription = '
	<div class="object-info">
		<div class="object-info__title">Санкт-Петербург Стачек</div>
		<div class="object-info__address">Стачек пр., 45/2 (пересечение Ново-Овсянниковской ул. и Баррикадной ул.)</div>
		<div class="object-info__text">
			<i class="ico-ring" style="color:red;"></i> Кировский завод
		</div>
		<div class="object-info__subtitle">График работы</div>
		<div class="object-info__schedule">
			<div class="object-info__schedule-item">
				<div class="object-info__schedule-day">пн</div>
				<div class="object-info__schedule-time"><span>09:00<br>18:00</span></div>
			</div>
			<div class="object-info__schedule-item">
				<div class="object-info__schedule-day">вт</div>
				<div class="object-info__schedule-time"><span>09:00<br>18:00</span></div>
			</div>
			<div class="object-info__schedule-item">
				<div class="object-info__schedule-day">ср</div>
				<div class="object-info__schedule-time"><span>09:00<br>18:00</span></div>
			</div>
			<div class="object-info__schedule-item">
				<div class="object-info__schedule-day">чт</div>
				<div class="object-info__schedule-time"><span>09:00<br>18:00</span></div>
			</div>
			<div class="object-info__schedule-item is-active">
				<div class="object-info__schedule-day">пт</div>
				<div class="object-info__schedule-time"><span>09:00<br>18:00</span></div>
			</div>
			<div class="object-info__schedule-item object-info__schedule-item_output">
				<div class="object-info__schedule-day">сб</div>
				<div class="object-info__schedule-time"><span>-</span></div>
			</div>
			<div class="object-info__schedule-item object-info__schedule-item_output">
				<div class="object-info__schedule-day">вс</div>
				<div class="object-info__schedule-time"><span>-</span></div>
			</div>
		</div>
		<div class="object-info__subtitle">Контакты</div>
		<div class="object-info__text">8 (812) 495 - 54 - 36</div>
		<div class="object-info__subtitle">Хранение заказа</div>
		<div class="object-info__text">Заказ хранится в магазине в течении 3х дней</div>
	</div>
';



$jsonFalse = '{
	"status": false,
	"message": "Неверные логин или пароль"
}';

$jsonTrue = '{
	"status": true
}';


if ($data['auth-login'] == 'admin' && $data['auth-pass'] == 'admin') {
	$json = $jsonTrue;
} else {
	$json = $jsonFalse;
}

echo $json;
?>